/* forEach - метод, позволяющий последовательно перебирать все элементы массива, для дальнейшей
их обработки. Вызывается через точечную нотацию после имени массива.
*/

/* Домашнее задание реализовано двумя вариантами исполнения
Вариант первый, применяющий классический функционал:
 */
const oldArray = ['hello', 'world', 23, '23', null];
function filterBy(array, type){
     const newArray = [];
     oldArray.forEach(
         function (elem) {
         if (typeof elem != type) {
             newArray.push(elem);
         }
     })
     return newArray;
}

alert(filterBy(oldArray, "string"));


// Второй вариант, с применением стрелочной функции и метода filter:
const oldArray = ['hello', 'world', 23, '23', null, 56, 101];
const filterBy = (array, type) => {
    return array.filter(elem => typeof elem !== type);
}
alert(filterBy(oldArray, "string"));