/* Экранирование - это прием подстановки управляющих символов на текстовые,
когда это необходимо в контексте */

const firstName = prompt("Please, enter your firstname: ");
const lastName = prompt("Please, enter your lastname: " );
const birthday = prompt("Please, enter your birthday's day:", "dd.mm.yyyy:");

function createNewUser (name, surname, birth) {
    const user = {
        name,
        surname,
        birth,
        getAge(){
            return new Date().getFullYear()- this.birth.slice(-4);
        },
        getPassword() {
            return this.name[0].toUpperCase() + this.surname.toLowerCase()
                + this.birth.slice(-4);
        },
        getLogin() {
            return (this.name[0] + this.surname).toLowerCase();
        }
    }
    return user;
}

alert(createNewUser(firstName, lastName, birthday).getLogin());
alert(createNewUser(firstName, lastName, birthday).getPassword());
alert(createNewUser(firstName, lastName, birthday).getAge());
