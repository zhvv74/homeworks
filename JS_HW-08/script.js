/* Обработчик событий - это инструмент, позволяющий отслеживать любые изменения в поведении мыши,
клавиатуры или сенсорных экранов пользователем. Это дает возможным применять какой-либо функционал
на эти изменения.
 */

const input = document.createElement("input");
input.classList.add("base-active");
document.body.append(input);
const span = document.createElement("span");
span.textContent = "price";
document.body.append(span);
input.addEventListener("focus", function (event) {
    event.target.classList.add("current-active");
});
input.addEventListener("blur", function () {
    input.classList.remove("current-active");
        const value = Number(input.value); // или + , это прииведение типа к числовому значению
        if (!value || Number.isNaN(value)) {
            if (!document.querySelector(".error-message")){
                const span = document.createElement("span");
                span.classList.add("error-message");
            span.textContent = "Please enter correct price";
            document.body.append(span);
            input.value = "";
            }
        } else if (value < 0){
            input.classList.add("error-active")
        }
        else {
            if (document.querySelector(".error-message")){
                document.querySelector(".error-message").remove();
            }
            const div = document.createElement("div");
            const span = document.createElement("span");
            span.textContent = ` Текущая цена: ${value} `;
            span.classList.add("valid-message");
            const button = document.createElement("button");
            button.textContent = "x";
            div.append(span, button);
            button.addEventListener("click", function (event){
               event.target.closest("div").remove();
            })
            document.body.append(div);
            input.insertAdjacentElement("beforebegin", div);
            input.value = "";
        }
    })


