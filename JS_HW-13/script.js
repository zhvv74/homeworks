const themeButton = document.querySelector(".theme-button");
const link = document.createElement("link");
link.setAttribute("rel", "stylesheet");

if(localStorage.getItem("pathLink")){
    link.setAttribute('href',localStorage.getItem("pathLink") )
    document.head.append(link);
}

themeButton.addEventListener("click", function (){
    if(localStorage.getItem("pathLink")){
        localStorage.removeItem("pathLink")
        link.remove()
    } else {
        localStorage.setItem("pathLink", "styleNew.css");
        link.setAttribute('href',localStorage.getItem("pathLink") )
        document.head.append(link);
    }
});