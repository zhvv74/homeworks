const form = document.querySelector(".password-form");
const firstInput = document.querySelector("#first");
const secondInput = document.querySelector("#second");
form.addEventListener("click", function (event){
    event.preventDefault();
    if(event.target.classList.contains("fa-eye")){
        event.target.classList.replace("fa-eye", "fa-eye-slash");
        event.target.closest('label').childNodes[1].type = 'password'
    } else if (event.target.classList.contains("fa-eye-slash")) {
        event.target.classList.replace("fa-eye-slash", "fa-eye");
        event.target.closest('label').childNodes[1].type = 'text'
    }
    if(event.target.classList.contains('btn')){
        if(firstInput.value === secondInput.value){
            firstInput.value = ''
            secondInput.value = ''
            alert("You are welcome");
        } else {
            firstInput.value = ''
            secondInput.value = ''
            alert("Нужно ввести одинаковые значения")
        }
    }
})
 