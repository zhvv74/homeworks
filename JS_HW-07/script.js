/* Document object model (DOM) - интерфейс, посредством которого можно управлять и придавать
функциональность HTML-разметке с помощью Javascript. Теперь HTML-разметка рассматривается в виде
объектов.
*/


const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function getArray(arr, parent = document.body){
    const ul = document.createElement("ul");
    parent.append(ul);
    const list = array.map(function (elem){
       return `<li>${elem}</li>`;
    })
    parent.insertAdjacentHTML("afterbegin", `<ul>${list.join("")}</ul>`
    );}

    getArray(array);


