/* События клавиатуры имеют свои собственные свойства - keydown, keyup и keypress.
Но эти события не могут учитывать вырезку/вставку текста мышей, поэтому применять
события клавиатуры для полей ввода в input нельзя или крайне нежелательно
 */

const buttons = document.querySelectorAll(".btn");
document.addEventListener("keydown", function (event){
    buttons.forEach(function (elem){
        if(event.code === elem.getAttribute("data-btn")){
            elem.classList.add("active-btn");
        } else { elem.classList.remove("active-btn");
        }
    })
})