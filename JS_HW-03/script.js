/* Функция, это определенный блок кода, в котором обрабатывается какое-либо действие, чтобы в дальнейшем применять этот
алгоритм в разных частях программы, вызывая лишь имя соответствующей функции. Т. е., функцию можно назвать заготовкой,
которая позволит избежать дублирования однотипных математических или других вычислений и применяться столько раз, сколько это
потребует код программы, что в свою очередь значительно сократит и повысит его качество.
   Функции могут быть с параметрами и без. Т. е. обрабатывать какой либо алгоритм и ждать его применения по вызову или
принимать на вход параметры и возвращать результат обработки с этими парамерами уже в самой функции, параметры которой
в процессе обработки уже становятся аргументами, т. е. с теми реальными значениями, которые буду подставляться в параметры
такой функции.
    И самое важное - при необходимости смены каких-либо однотипных команд, их не нужно везде менять, а только в самой
функции, к которой обращаются эти команды. Т. е., один раз и для всех сразу.

*/

let a = +prompt("Please, enter first value:");
while(Number.isNaN(a)){
    a = +prompt("Please, enter only numbers")
}
let b = +prompt("Please, enter second value:");
while(Number.isNaN(b)){
    b = +prompt("Please, enter only numbers")
}
let operator = prompt("Please, enter operator");
while (operator !== "+" && operator !== "-" && operator !== "*" && operator !== "/"){
    operator = prompt("Enter correct operator");
}

doMath();

function doMath() {
    let result;
    let add;
    let sub;
    let mul;
    let div;
    if (operator === "+") {
        add = a + b;
    } else if (operator === "-") {
        sub = a - b;
    } else if (operator === "*") {
        mul = a * b;
    } else if (operator === "/") {
        div = a / b;
    } else {
        alert("Please, enter required operator")
    }
    switch (operator) {
        case "+":
            result = add;
            break;
        case "-":
            result = sub;
            break;
        case "*":
            result = mul;
            break;
        case "/":
            result = div;
            break;
        default:
            alert("Enter one of next operators: "+" "-" "*" "/"");
    }
    alert("Result of operation is: " + result);
}





