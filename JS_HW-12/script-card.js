/* Функции setTimeout() и setInterval() нужны для планирования вызова других функций, т. е. через определенное время
они будут срабатывать. Разница между ними в том. что setTimeout() сработает только один раз, а
setInterval() будет срабатывать периодически через заданное время.
   Если функции setTimeout() задать нулевую задержку она сработает мгновенно, но только после отрабатывания всего
остального кода из-за ассинхронности.
   Функция clearInterval() нужна для отмены двух предыдущих функций, иначе срабатывание отработанной функции
продолжится, что нарушит дальнейшую логику кода. */


const images = document.querySelectorAll(".image-to-show");
let index = 0;
let timerId;
function showImages(){
    images[index].classList.add("invisible");
    index ++;
    if(images.length === index){
        index = 0;
    }
    images[index].classList.remove("invisible");
}
const btn = document.querySelector(".buttons");
btn.addEventListener("click", function (event){
    if(event.target.id === "break"){
        removeInterval();
    } else if (event.target.id === "continue"){
        createInterval();
    }
})
function createInterval(){
    timerId = setInterval(showImages, 3000);
}
function  removeInterval(){
    clearInterval(timerId);
}

createInterval();


